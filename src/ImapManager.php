<?php

namespace Usertech\Imap;

use stdClass;
use Usertech\Imap\Exceptions\IncorrectMailboxException;
use Usertech\Imap\Exceptions\FailedToOpenStreamException;
use Usertech\Imap\Exceptions\IncorrectImapCredentialsException;


class ImapManager {

	protected $imapBasePath;
	protected $mailboxPath = '';
	protected $imapLogin;
	protected $imapPassword;
	protected $imapOptions = 0;
	protected $imapRetriesNum = 0;
	protected $imapParams = [];
	protected $serverEncoding;
	protected $existingFolders = [];
	protected $scFolders;
	protected $subFoldersSupport = NULL;
	protected $operatingDir = NULL;
	protected $tempMailDir;
	protected $folderDelimiter;

	/**
	 * 
	 * @param string $domain gmail.com
	 * @param int $port 993
	 * @param string $login email.name
	 * @param string $password xxx
	 * @param string $protocol ssl
	 * @param string $tempMailDir abslute path to dir where .EML mails will be saved
	 * @param boolean|NULL $supportSubFolders NULL will cause autodetect
	 * @param string $operatingDir defines dir that will be added as parent dir to every structure that comes to this class 
	 * methods but only if mailbox supports subfolders. Method dressStructure takes care fo this process.
	 * @param string $serverEncoding
	 */
	public function __construct($domain, $port, $login, $password, $protocol = 'ssl', $tempMailDir, $supportSubFolders = NULL, $operatingDir = NULL, $checkSubfolderStructure = [], $serverEncoding = 'UTF-8') {
		$this->imapBasePath = '{' . $domain . ':' . $port . '/imap/' . strtolower($protocol) . '}';
		$this->imapLogin = $login;
		$this->imapPassword = $password;
		$this->serverEncoding = strtoupper($serverEncoding);
		$this->tempMailDir = $tempMailDir;
		$this->folderDelimiter = $this->getFolderDelimiter();
		if(!self::testConnection($domain, $port, $login, $password)){
			throw new IncorrectImapCredentialsException('Unable to connect to mailbox.');
		}
		if ($supportSubFolders === NULL) {
			$this->subFoldersSupport = $this->detectSupportOfSubfolders($checkSubfolderStructure);
		} else {
			$this->subFoldersSupport = $supportSubFolders;
		}
		$this->operatingDir = $this->secureDirName($operatingDir);
	}

	protected function getFolderDelimiter() {
		$delimiter = '/';
		$info = $this->getFoldersInfo();
		if (!empty($info[0]) && !empty($info[0]->delimiter)) {
			$delimiter = $info[0]->delimiter;
		}
		return $delimiter;
	}

	public static function testConnection($domain, $port, $login, $password, $protocol = 'ssl') {
		$result = (bool) @imap_open('{' . $domain . ':' . $port . '/imap/' . strtolower($protocol) . '}', $login, $password);
		imap_errors();
		return $result;
	}

	/**
	 * Set custom connection arguments of imap_open method. See http://php.net/imap_open
	 * @param int $options
	 * @param int $retriesNum
	 * @param array $params
	 */
	public function setConnectionArgs($options = 0, $retriesNum = 0, array $params = null) {
		$this->imapOptions = $options;
		$this->imapRetriesNum = $retriesNum;
		$this->imapParams = $params;
	}

	/**
	 * Get IMAP mailbox connection stream
	 * @param bool $forceConnection Initialize connection if it's not initialized
	 * @return null|resource
	 */
	public function getImapStream($forceConnection = true) {
		static $imapStream;
		if ($forceConnection) {
			if ($imapStream && (!is_resource($imapStream) || !imap_ping($imapStream))) {
				$this->disconnect();
				$imapStream = null;
			}
			if (!$imapStream) {
				$imapStream = $this->initImapStream();
			}
		}
		return $imapStream;
	}

	/**
	 * 
	 * @param string $mailboxPath
	 * @param string|NULL $encodeFrom
	 * @return string Last error
	 * @throws IncorrectMailboxException
	 */
	public function setMailbox($mailboxPath = '', $encodeFrom = NULL) {
		imap_errors();
		$this->mailboxPath = $this->prepareFolderName($mailboxPath, $encodeFrom);

		imap_reopen($this->getImapStream(), $this->mailboxPath);

		$last_error = imap_last_error();
		imap_errors();
		if (!empty($last_error)) {
			// if error, set mailbox back to basepath
			imap_reopen($this->getImapStream(), $this->imapBasePath);
			if (strpos($last_error, '[NONEXISTENT] Unknown Mailbox') !== false) {
				throw new IncorrectMailboxException('Directory "' . $this->mailboxPath . '" does not exists in mailbox');
			} else {
				throw new \Exception('Unexpected error: "' . $last_error . '"');
			}
		}
	}

	/**
	 * Finds out wherher given structure exists.
	 * @param string $mailboxPath
	 * @param string|NULL $encodeFrom
	 * @return boolean
	 * @throws Exception
	 */
	public function existsStructureInMailbox($structure) {
		try{
			$structure = $this->dressStructure($structure);
			$stringFolders = $this->foldersToPaths($structure);
			$stringFolder = end($stringFolders);
			$folderName = $this->prepareFolderName($stringFolder);
			imap_reopen($this->getImapStream(), $folderName);
			$last_error = imap_last_error();
			imap_errors();
			imap_reopen($this->getImapStream(), $this->imapBasePath);
		} catch (Exception $e){
			return false;
		}
		return empty($last_error);
	}

	/**
	 * Secures all dir names and adds operating dir if needed
	 * @param array $structure
	 * @param string $encodeFrom
	 * @throws IncorrectMailboxException
	 */
	protected function dressStructure($structure = []) {
		$structure = $this->secureStructure($structure);
		if (!empty($this->operatingDir) && $this->isSubFolderSupporting()) {
			return [$this->operatingDir => $structure];
		}
		return $structure;
	}

	/**
	 * Secures all dir names in structure - replace all slashes
	 * @param array $structure
	 * @return array
	 */
	protected function secureStructure($structure = []) {
		$return = [];
		foreach ($structure as $key => $value) {
			$key = $this->secureDirName($key);
			if (is_array($value)) {
				$value = $this->secureStructure($value);
			}
			$return[$key] = $value;
		}
		return $return;
	}

	/**
	 * Secures dir name - replace all slashes
	 * @param string $name
	 * @return string
	 */
	protected function secureDirName($name) {
		$nameSecured = str_replace('/', '-', $name);
		return $nameSecured;
	}

	/**
	 * 
	 * @param array $structure
	 * @param string $encodeFrom
	 * @throws IncorrectMailboxException
	 */
	public function setMailboxByStructure($structure = [], $encodeFrom = NULL) {
		$dressedStructure = $this->dressStructure($structure);
		$paths = $this->foldersToPaths($dressedStructure, '/', $this->isSubFolderSupporting(), true, $encodeFrom);
		$stringPath = '';
		if (is_array($paths)) {
			$stringPath = reset($paths);
		}
		$this->setMailbox($stringPath);
	}

	/**
	 * @return resource
	 * @throws IncorrectImapCredentialsException
	 */
	protected function initImapStream() {
		$imapStream = @imap_open($this->mailboxPath ? $this->mailboxPath : $this->imapBasePath, $this->imapLogin, $this->imapPassword, $this->imapOptions, $this->imapRetriesNum, $this->imapParams);
		if (!$imapStream) {
			$last_error = imap_last_error();
			imap_errors();
			throw new IncorrectImapCredentialsException('Connection error: ' . $last_error);
		}
		return $imapStream;
	}

	protected function disconnect() {
		$imapStream = $this->getImapStream(false);
		if ($imapStream && is_resource($imapStream)) {
			imap_close($imapStream, CL_EXPUNGE);
		}
	}

	/**
	 * Get information about the current mailbox.
	 *
	 * Returns the information in an object with following properties:
	 *  Date - current system time formatted according to RFC2822
	 *  Driver - protocol used to access this mailbox: POP3, IMAP, NNTP
	 *  Mailbox - the mailbox name
	 *  Nmsgs - number of mails in the mailbox
	 *  Recent - number of recent mails in the mailbox
	 *
	 * @return stdClass
	 */
	public function checkMailbox() {
		return imap_check($this->getImapStream());
	}

	/**
	 * Detects if IMAP server supports subfolders
	 * @return bool
	 */
	protected function detectSupportOfSubfolders($checkSubfolderStructure) {
		if (empty($checkSubfolderStructure)) {
			$checkSubfolderStructure = [
				'test' => [
					'subtest' => []
				]
			];
		}
		$toDeleteFolders = [];
		foreach ($this->foldersToPaths($checkSubfolderStructure) as $folderToAdd) {
			$this->addFolder($folderToAdd);
			$toDeleteFolders[] = $folderToAdd;
		}
		imap_errors();
		$this->subFoldersSupport = $this->existsStructureInMailbox($checkSubfolderStructure);
		if (!$this->subFoldersSupport) {
			foreach ($toDeleteFolders as $toDeleteFolder) {
				$this->deleteFolderByPath($toDeleteFolder);
			}
		}
		return $this->subFoldersSupport;
	}

	/**
	 * @return bool
	 */
	public function isSubFolderSupporting() {
		return $this->subFoldersSupport;
	}

	/**
	 * Creates folders in mailbox by given structure 
	 * @param array $structure folders to add 
	 * @param string $atDir
	 * @return array of paths to folders (deepest ones, including those that already exists)
	 */
	public function addFolders($structure, $atDir = '/') {
		if ($atDir === '/') {
			$structure = $this->dressStructure($structure);
		}
		$existingFolders = $this->getFolders();
		$folderPaths = $this->foldersToPaths($structure, $atDir, $this->isSubFolderSupporting(), !$this->isSubFolderSupporting());
		$foldersToAdd = array_diff($folderPaths, $existingFolders);
		foreach ($foldersToAdd as $folderToAdd) {
			$this->addFolder($folderToAdd);
		}
		return $this->foldersToPaths($structure, $atDir, $this->isSubFolderSupporting(), true);
	}

	/**
	 * Returns flat array with paths to folders depending on subfolder support
	 * @param array $folders
	 * @param string $atDir
	 * @param bool $isSubFolderSupporting
	 * @param bool $onlyDeepest
	 * @return array 
	 */
	protected function foldersToPaths($folders, $atDir = '/', $isSubFolderSupporting = true, $onlyDeepest = false, $fromEncoding = NULL) {
		if (empty($folders) || !is_array($folders)) {
			return [];
		}
		$folderPaths = [];
		$delimiter = $isSubFolderSupporting ? $this->folderDelimiter : ' - ';
		foreach ($folders as $folder => $subdirs) {
			$folderPath = trim(trim($atDir . $folder, $delimiter), '/');
			if (!$onlyDeepest || ($onlyDeepest && empty($subdirs))) {

				$folderPaths[] = $fromEncoding ? mb_convert_encoding($folderPath, "UTF7-IMAP", "UTF-8") : mb_convert_encoding($folderPath, "UTF7-IMAP");
			}
			$folderPaths = array_merge($folderPaths, $this->foldersToPaths($subdirs, $atDir . $folder . $delimiter, $isSubFolderSupporting, $onlyDeepest));
		}
		return $folderPaths;
	}

	/**
	 * Creates new folder in mailbox
	 * @param string $string
	 * @param string $encodeFrom
	 * @return bool
	 */
	public function addFolder($string, $encodeFrom = NULL) {
		$prepared = $this->prepareFolderName($string, $encodeFrom);
		imap_createmailbox($this->getImapStream(), $prepared);
		imap_subscribe($this->getImapStream(), $prepared);
	}

	/**
	 * Deletes first folder in given structure in mailbox
	 * @param array $structure
	 * @param string $encodeFrom
	 * @param string $onlyForSubFolderSupporting
	 * @return bool
	 */
	public function deleteFolder($structure, $encodeFrom = "UTF-8", $onlyForSubFolderSupporting = false) {
		if (!$onlyForSubFolderSupporting || $this->isSubFolderSupporting()) {
			$structure = $this->dressStructure($structure);
			$stringFolders = $this->foldersToPaths($structure);
			$stringFolder = end($stringFolders);
			$folderName = $this->prepareFolderName($stringFolder, $encodeFrom);
			return imap_deletemailbox($this->getImapStream(), $folderName);
		}
		return false;
	}

	/**
	 * Deletes specific folder in mailbox
	 * @param string $string
	 * @param string $encodeFrom
	 * @return bool
	 */
	public function deleteFolderByPath($path, $encodeFrom = "UTF-8", $onlyForSubFolderSupporting = false) {
		$return = false;
		if (!$onlyForSubFolderSupporting || $this->isSubFolderSupporting()) {
			$return = imap_deletemailbox($this->getImapStream(), $this->prepareFolderName($path, $encodeFrom));
		}
		return $return;
	}

	/**
	 * @param int $message_uid
	 * @return string
	 * @throws FailedToOpenStreamException
	 */
	public function saveEml($message_uid) {
		$message_number = imap_msgno($this->getImapStream(), $message_uid);
		$random_name = bin2hex(mcrypt_create_iv(22, MCRYPT_DEV_URANDOM));
		$savePath = $this->tempMailDir . '/eml-' . $random_name . '.eml';
		$headers = imap_fetchheader($this->getImapStream(), $message_number, FT_PREFETCHTEXT);
		$body = imap_body($this->getImapStream(), $message_number);
		if (@file_put_contents($savePath, $headers . "\n" . $body) === false) {
			throw new FailedToOpenStreamException("Unable to save file '$savePath', probably because of failed to open stream: Permission denied.");
		}
		return $savePath;
	}

	/**
	 * Renames specific folder in mailbox
	 * @param array $from structure ['DIR1' => ['DIR2' => []]]
	 * @param array $to structure ['DIR1' => ['DIR2' => []]]
	 * @param string $encodeFrom
	 * @return string
	 * @throws IncorrectMailboxException
	 */
	public function renameFolder($from, $to, $deleteOldParentPathIfEmpty = true, $encodeFrom = NULL) {
		$status = '';
		$from = $this->dressStructure($from);
		$to = $this->dressStructure($to);
		$fromPaths = $this->foldersToPaths($from, '/', $this->isSubFolderSupporting(), true);
		$fromPath = $this->prepareFolderName(reset($fromPaths), $encodeFrom);
		$toPaths = $this->foldersToPaths($to, '/', $this->isSubFolderSupporting(), true);
		$toPath = $this->prepareFolderName(reset($toPaths), $encodeFrom);
		if ($fromPath == $toPath) {
			return 'OK. Target path is same as source path.' . PHP_EOL;
		}
		$success = imap_renamemailbox($this->getImapStream(), $fromPath, $toPath);
		if (!$success || strpos(imap_last_error(), '[ALREADYEXISTS] Duplicate folder name') !== false) {
			$this->addFolder($toPath);
			$i = 0;
			do {
				imap_errors();
				$this->setMailbox($fromPath);
				$mails = $this->searchMailbox();
				$toPathPrepared = $this->prepareFolderName($toPath, NULL, false);
				foreach ($mails as $mailId) {
					//TODO CARE OF NON EXISTENT DESTINATIN ERROR, but it should not happen
					imap_mail_move($this->getImapStream(), "1:*", $toPathPrepared) . PHP_EOL;
				}
			} while (count($mails) && $i++ < 10);
			if ($i >= 4) {
				$status .= imap_last_error() . PHP_EOL;
				imap_errors();
			}
			$this->deleteFolderByPath($fromPath, NULL);
		}
		$status .= imap_last_error() . PHP_EOL;
		imap_errors();
		if ($deleteOldParentPathIfEmpty && $this->isSubFolderSupporting()) {
			$parentPath = substr($fromPath, 0, strrpos($fromPath, '/'));
			$emptyFolders = empty($this->getFolders($parentPath . '/%'));
			$this->setMailbox($parentPath);
			$countMails = $this->countMails();
			$this->setMailbox('');
			if ($emptyFolders && !$countMails) {
				$this->deleteFolderByPath($parentPath, NULL);
			}
		}

		if (empty($status)) {
			$status = 'OK';
		}
		return $status;
	}

	/**
	 * Checks base part of mailbox path and adds if needed and encode string to UTF7-IMAP
	 * @param string $name
	 * @param string $encodeFrom
	 * @return bool
	 */
	public function prepareFolderName($name, $encodeFrom = NULL, $includeBasePath = true) {
		if ($includeBasePath) {
			$baseName = strpos($name, $this->imapBasePath) === false ? $this->imapBasePath . $name : $name;
		} else {
			$baseName = str_replace($this->imapBasePath, '', $name);
		}
		return is_string($encodeFrom) ? mb_convert_encoding($baseName, "UTF7-IMAP", $encodeFrom) : $baseName;
	}

	/**
	 * Gets status information about the given mailbox.
	 *
	 * This function returns an object containing status information.
	 * The object has the following properties: messages, recent, unseen, uidnext, and uidvalidity.
	 *
	 * @return stdClass if the box doesn't exist
	 */
	public function statusMailbox() {
		return imap_status($this->getImapStream(), $this->imapBasePath, SA_ALL);
	}

	public function getLsub() {
		$folders = imap_lsub($this->getImapStream(), $this->imapBasePath, "*");
		return $folders;
	}

	/**
	 * Detects if mailbox has noinferiors flag (can't store subfolders)
	 * @param string $lsub_line mailbox line from untagged LIST or LSUB response
	 * @return bool whether this is a Noinferiors mailbox.
	 * @since 1.5.0
	 */
	function check_is_noinferiors($lsub_line) {
		return preg_match("/^\* (LSUB|LIST) \([^\)]*\\\\Noinferiors[^\)]*\)/i", $lsub_line);
	}

	public function getMailHeader($mailId) {
		$folders = imap_headerinfo($this->getImapStream(), $mailId);
		return $folders;
	}

	public function getFolders($pattern = '*') {
		$folders = imap_list($this->getImapStream(), $this->imapBasePath, $pattern);
		return str_replace($this->imapBasePath, "", $folders);
	}

	public function getFoldersInfo($pattern = '*') {
		$folders = imap_getmailboxes($this->getImapStream(), $this->imapBasePath, $pattern);
		return str_replace($this->imapBasePath, "", $folders);
	}

	public function getMailSeq($mailId) {
		$folders = imap_msgno($this->getImapStream(), $mailId);
		return $folders;
	}

	public function getMailUid($mailSeq) {
		$folders = imap_uid($this->getImapStream(), $mailSeq);
		return $folders;
	}

	/**
	 * Gets listing the folders
	 *
	 * This function returns an object containing listing the folders.
	 * The object has the following properties: messages, recent, unseen, uidnext, and uidvalidity.
	 *
	 * @return array listing the folders
	 */
	public function getListingFolders() {
		$folders = imap_list($this->getImapStream(), $this->imapBasePath, "*");
		foreach ($folders as $key => $folder) {
			$folder = str_replace($this->imapBasePath, "", imap_utf7_decode($folder));
			$folders[$key] = $folder;
		}
		return $folders;
	}

	/**
	 * This function performs a search on the mailbox currently opened in the given IMAP stream.
	 * For example, to match all unanswered mails sent by Mom, you'd use: "UNANSWERED FROM mom".
	 * Searches appear to be case insensitive. This list of criteria is from a reading of the UW
	 * c-client source code and may be incomplete or inaccurate (see also RFC2060, section 6.4.4).
	 *
	 * @param string $criteria String, delimited by spaces, in which the following keywords are allowed. Any multi-word arguments (e.g. FROM "joey smith") must be quoted. Results will match all criteria entries.
	 *    ALL - return all mails matching the rest of the criteria
	 *    ANSWERED - match mails with the \\ANSWERED flag set
	 *    BCC "string" - match mails with "string" in the Bcc: field
	 *    BEFORE "date" - match mails with Date: before "date"
	 *    BODY "string" - match mails with "string" in the body of the mail
	 *    CC "string" - match mails with "string" in the Cc: field
	 *    DELETED - match deleted mails
	 *    FLAGGED - match mails with the \\FLAGGED (sometimes referred to as Important or Urgent) flag set
	 *    FROM "string" - match mails with "string" in the From: field
	 *    KEYWORD "string" - match mails with "string" as a keyword
	 *    NEW - match new mails
	 *    OLD - match old mails
	 *    ON "date" - match mails with Date: matching "date"
	 *    RECENT - match mails with the \\RECENT flag set
	 *    SEEN - match mails that have been read (the \\SEEN flag is set)
	 *    SINCE "date" - match mails with Date: after "date"
	 *    SUBJECT "string" - match mails with "string" in the Subject:
	 *    TEXT "string" - match mails with text "string"
	 *    TO "string" - match mails with "string" in the To:
	 *    UNANSWERED - match mails that have not been answered
	 *    UNDELETED - match mails that are not deleted
	 *    UNFLAGGED - match mails that are not flagged
	 *    UNKEYWORD "string" - match mails that do not have the keyword "string"
	 *    UNSEEN - match mails which have not been read yet
	 *
	 * @return array Mails ids
	 */
	public function searchMailbox($criteria = 'ALL') {
		$mailsIds = imap_search($this->getImapStream(), $criteria, SE_UID, 'UTF-8');
		return $mailsIds ? $mailsIds : [];
	}

	/**
	 * Save mail body.
	 * @return bool
	 */
	public function saveMail($mailId, $filename = 'email.eml') {
		return imap_savebody($this->getImapStream(), $filename, $mailId, "", FT_UID);
	}

	/**
	 * Marks mails listed in mailId for deletion.
	 * @return bool
	 */
	public function deleteMail($mailId) {
		return imap_delete($this->getImapStream(), $mailId, FT_UID);
	}

	public function moveMail($mailId, $mailBox) {
		return imap_mail_move($this->getImapStream(), $mailId, $mailBox, CP_UID) && $this->expungeDeletedMails();
	}

	/**
	 * Deletes all the mails marked for deletion by imap_delete(), imap_mail_move(), or imap_setflag_full().
	 * @return bool
	 */
	public function expungeDeletedMails() {
		return imap_expunge($this->getImapStream());
	}

	/**
	 * Add the flag \Seen to a mail.
	 * @return bool
	 */
	public function markMailAsRead($mailId) {
		return $this->setFlag(array($mailId), '\\Seen');
	}

	/**
	 * Remove the flag \Seen from a mail.
	 * @return bool
	 */
	public function markMailAsUnread($mailId) {
		return $this->clearFlag(array($mailId), '\\Seen');
	}

	/**
	 * Add the flag \Flagged to a mail.
	 * @return bool
	 */
	public function markMailAsImportant($mailId) {
		return $this->setFlag(array($mailId), '\\Flagged');
	}

	/**
	 * Add the flag \Seen to a mails.
	 * @return bool
	 */
	public function markMailsAsRead(array $mailId) {
		return $this->setFlag($mailId, '\\Seen');
	}

	/**
	 * Remove the flag \Seen from some mails.
	 * @return bool
	 */
	public function markMailsAsUnread(array $mailId) {
		return $this->clearFlag($mailId, '\\Seen');
	}

	/**
	 * Add the flag \Flagged to some mails.
	 * @return bool
	 */
	public function markMailsAsImportant(array $mailId) {
		return $this->setFlag($mailId, '\\Flagged');
	}

	/**
	 * Causes a store to add the specified flag to the flags set for the mails in the specified sequence.
	 *
	 * @param array $mailsIds
	 * @param string $flag which you can set are \Seen, \Answered, \Flagged, \Deleted, and \Draft as defined by RFC2060.
	 * @return bool
	 */
	public function setFlag(array $mailsIds, $flag) {
		return imap_setflag_full($this->getImapStream(), implode(',', $mailsIds), $flag, ST_UID);
	}

	/**
	 * Cause a store to delete the specified flag to the flags set for the mails in the specified sequence.
	 *
	 * @param array $mailsIds
	 * @param string $flag which you can set are \Seen, \Answered, \Flagged, \Deleted, and \Draft as defined by RFC2060.
	 * @return bool
	 */
	public function clearFlag(array $mailsIds, $flag) {
		return imap_clearflag_full($this->getImapStream(), implode(',', $mailsIds), $flag, ST_UID);
	}

	/**
	 * Fetch mail headers for listed mails ids
	 *
	 * Returns an array of objects describing one mail header each. The object will only define a property if it exists. The possible properties are:
	 *  subject - the mails subject
	 *  from - who sent it
	 *  to - recipient
	 *  date - when was it sent
	 *  message_id - Mail-ID
	 *  references - is a reference to this mail id
	 *  in_reply_to - is a reply to this mail id
	 *  size - size in bytes
	 *  uid - UID the mail has in the mailbox
	 *  msgno - mail sequence number in the mailbox
	 *  recent - this mail is flagged as recent
	 *  flagged - this mail is flagged
	 *  answered - this mail is flagged as answered
	 *  deleted - this mail is flagged for deletion
	 *  seen - this mail is flagged as already read
	 *  draft - this mail is flagged as being a draft
	 *
	 * @param array $mailsSeqNums
	 * @return array
	 */
	public function getMailsInfo(array $mailsSeqNums) {
		$mails = imap_fetch_overview($this->getImapStream(), implode(',', $mailsSeqNums), FT_UID);
		if (is_array($mails) && count($mails)) {
			foreach ($mails as &$mail) {
				if (isset($mail->subject)) {
					$mail->subject = $this->decodeMimeStr($mail->subject, $this->serverEncoding);
				}
				if (isset($mail->from)) {
					$mail->from = $this->decodeMimeStr($mail->from, $this->serverEncoding);
				}
				if (isset($mail->to)) {
					$mail->to = $this->decodeMimeStr($mail->to, $this->serverEncoding);
				}
			}
		}
		return $mails;
	}

	/**
	 * 
	 * @param array $mailsSeqNums
	 * @return array
	 */
	public function getMessageUniqueIds(array $mailsSeqNums) {
		$message_unique_ids = [];
		$infos = $this->getMailsInfo($mailsSeqNums);
		foreach ($infos as $info) {
			if (empty($info->message_id)) {
				continue;
			}
			$message_unique_ids[$info->uid] = $info->message_id;
		}
		return $message_unique_ids;
	}

	/**
	 * Get information about the current mailbox.
	 *
	 * Returns an object with following properties:
	 *  Date - last change (current datetime)
	 *  Driver - driver
	 *  Mailbox - name of the mailbox
	 *  Nmsgs - number of messages
	 *  Recent - number of recent messages
	 *  Unread - number of unread messages
	 *  Deleted - number of deleted messages
	 *  Size - mailbox size
	 *
	 * @return object Object with info | FALSE on failure
	 */
	public function getMailboxInfo() {
		return imap_mailboxmsginfo($this->getImapStream());
	}

	/**
	 * Gets mails ids sorted by some criteria
	 *
	 * Criteria can be one (and only one) of the following constants:
	 *  SORTDATE - mail Date
	 *  SORTARRIVAL - arrival date (default)
	 *  SORTFROM - mailbox in first From address
	 *  SORTSUBJECT - mail subject
	 *  SORTTO - mailbox in first To address
	 *  SORTCC - mailbox in first cc address
	 *  SORTSIZE - size of mail in octets
	 *
	 * @param int $criteria
	 * @param bool $reverse
	 * @return array Mails ids
	 */
	public function sortMails($criteria = SORTARRIVAL, $reverse = true) {
		return imap_sort($this->getImapStream(), $criteria, $reverse, SE_UID);
	}

	/**
	 * Get mails count in mail box
	 * @return int
	 */
	public function countMails() {
		return imap_num_msg($this->getImapStream());
	}

	/**
	 * Retrieve the quota settings per user
	 * @return array - FALSE in the case of call failure
	 */
	public function getQuota() {
		return imap_get_quotaroot($this->getImapStream(), 'INBOX');
	}

	/**
	 * Return quota limit in KB
	 * @return int - FALSE in the case of call failure
	 */
	public function getQuotaLimit() {
		$quota = $this->getQuota();
		if (is_array($quota)) {
			$quota = $quota['STORAGE']['limit'];
		}
		return $quota;
	}

	/**
	 * Return quota usage in KB
	 * @return int - FALSE in the case of call failure
	 */
	public function getQuotaUsage() {
		$quota = $this->getQuota();
		if (is_array($quota)) {
			$quota = $quota['STORAGE']['usage'];
		}
		return $quota;
	}

	/**
	 * Get mail data
	 *
	 * @param $mailId
	 * @param bool $markAsSeen
	 * @return IncomingMail
	 */
	public function getMail($mailId, $markAsSeen = true) {
		$head = imap_rfc822_parse_headers(imap_fetchheader($this->getImapStream(), $mailId, FT_UID));

		$mail = new IncomingMail();
		$mail->id = $mailId;
		$mail->date = date('Y-m-d H:i:s', isset($head->date) ? strtotime(preg_replace('/\(.*?\)/', '', $head->date)) : time());
		$mail->subject = isset($head->subject) ? $this->decodeMimeStr($head->subject, $this->serverEncoding) : null;
		$mail->fromName = isset($head->from[0]->personal) ? $this->decodeMimeStr($head->from[0]->personal, $this->serverEncoding) : null;
		$mail->fromAddress = strtolower($head->from[0]->mailbox . '@' . $head->from[0]->host);
		if (isset($head->to)) {
			$toStrings = [];
			foreach ($head->to as $to) {
				if (!empty($to->mailbox) && !empty($to->host)) {
					$toEmail = strtolower($to->mailbox . '@' . $to->host);
					$toName = isset($to->personal) ? $this->decodeMimeStr($to->personal, $this->serverEncoding) : null;
					$toStrings[] = $toName ? "$toName <$toEmail>" : $toEmail;
					$mail->to[$toEmail] = $toName;
				}
			}
			$mail->toString = implode(', ', $toStrings);
		}

		if (isset($head->cc)) {
			foreach ($head->cc as $cc) {
				$mail->cc[strtolower($cc->mailbox . '@' . $cc->host)] = isset($cc->personal) ? $this->decodeMimeStr($cc->personal, $this->serverEncoding) : null;
			}
		}

		if (isset($head->reply_to)) {
			foreach ($head->reply_to as $replyTo) {
				$mail->replyTo[strtolower($replyTo->mailbox . '@' . $replyTo->host)] = isset($replyTo->personal) ? $this->decodeMimeStr($replyTo->personal, $this->serverEncoding) : null;
			}
		}

		$mailStructure = imap_fetchstructure($this->getImapStream(), $mailId, FT_UID);

		if (empty($mailStructure->parts)) {
			$this->initMailPart($mail, $mailStructure, 0, $markAsSeen);
		} else {
			foreach ($mailStructure->parts as $partNum => $partStructure) {
				$this->initMailPart($mail, $partStructure, $partNum + 1, $markAsSeen);
			}
		}

		return $mail;
	}

	protected function initMailPart(IncomingMail $mail, $partStructure, $partNum, $markAsSeen = true) {
		$options = FT_UID;
		if (!$markAsSeen) {
			$options |= FT_PEEK;
		}
		$data = $partNum ? imap_fetchbody($this->getImapStream(), $mail->id, $partNum, $options) : imap_body($this->getImapStream(), $mail->id, $options);

		if ($partStructure->encoding == 1) {
			$data = imap_utf8($data);
		} elseif ($partStructure->encoding == 2) {
			$data = imap_binary($data);
		} elseif ($partStructure->encoding == 3) {
			$data = imap_base64($data);
		} elseif ($partStructure->encoding == 4) {
			$data = quoted_printable_decode($data);
		}

		$params = [];
		if (!empty($partStructure->parameters)) {
			foreach ($partStructure->parameters as $param) {
				$params[strtolower($param->attribute)] = $param->value;
			}
		}
		if (!empty($partStructure->dparameters)) {
			foreach ($partStructure->dparameters as $param) {
				$paramName = strtolower(preg_match('~^(.*?)\*~', $param->attribute, $matches) ? $matches[1] : $param->attribute);
				if (isset($params[$paramName])) {
					$params[$paramName] .= $param->value;
				} else {
					$params[$paramName] = $param->value;
				}
			}
		}

		// attachments
		$attachmentId = $partStructure->ifid ? trim($partStructure->id, " <>") : (isset($params['filename']) || isset($params['name']) ? mt_rand() . mt_rand() : null);

		if ($attachmentId) {
			if (empty($params['filename']) && empty($params['name'])) {
				$fileName = $attachmentId . '.' . strtolower($partStructure->subtype);
			} else {
				$fileName = !empty($params['filename']) ? $params['filename'] : $params['name'];
				$fileName = $this->decodeMimeStr($fileName, $this->serverEncoding);
				$fileName = $this->decodeRFC2231($fileName, $this->serverEncoding);
			}
			$attachment = new IncomingMailAttachment();
			$attachment->id = $attachmentId;
			$attachment->name = $fileName;
			if ($this->tempMailDir) {
				$replace = array(
					'/\s/' => '_',
					'/[^0-9a-zа-яіїє_\.]/iu' => '',
					'/_+/' => '_',
					'/(^_)|(_$)/' => '',
				);
				$fileSysName = preg_replace('~[\\\\/]~', '', $mail->id . '_' . $attachmentId . '_' . preg_replace(array_keys($replace), $replace, $fileName));
				$attachment->filePath = $this->tempMailDir . DIRECTORY_SEPARATOR . $fileSysName;
				file_put_contents($attachment->filePath, $data);
			}
			$mail->addAttachment($attachment);
		} else {
			if (!empty($params['charset'])) {
				$data = $this->convertStringEncoding($data, $params['charset'], $this->serverEncoding);
			}
			if ($partStructure->type == 0 && $data) {
				if (strtolower($partStructure->subtype) == 'plain') {
					$mail->textPlain .= $data;
				} else {
					$mail->textHtml .= $data;
				}
			} elseif ($partStructure->type == 2 && $data) {
				$mail->textPlain .= trim($data);
			}
		}
		if (!empty($partStructure->parts)) {
			foreach ($partStructure->parts as $subPartNum => $subPartStructure) {
				if ($partStructure->type == 2 && $partStructure->subtype == 'RFC822') {
					$this->initMailPart($mail, $subPartStructure, $partNum, $markAsSeen);
				} else {
					$this->initMailPart($mail, $subPartStructure, $partNum . '.' . ($subPartNum + 1), $markAsSeen);
				}
			}
		}
	}

	protected function decodeMimeStr($string, $charset = 'utf-8') {
		$newString = '';
		$elements = imap_mime_header_decode($string);
		for ($i = 0; $i < count($elements); $i++) {
			if ($elements[$i]->charset == 'default') {
				$elements[$i]->charset = 'iso-8859-1';
			}
			$newString .= $this->convertStringEncoding($elements[$i]->text, $elements[$i]->charset, $charset);
		}
		return $newString;
	}

	function isUrlEncoded($string) {
		$hasInvalidChars = preg_match('#[^%a-zA-Z0-9\-_\.\+]#', $string);
		$hasEscapedChars = preg_match('#%[a-zA-Z0-9]{2}#', $string);
		return !$hasInvalidChars && $hasEscapedChars;
	}

	protected function decodeRFC2231($string, $charset = 'utf-8') {
		if (preg_match("/^(.*?)'.*?'(.*?)$/", $string, $matches)) {
			$encoding = $matches[1];
			$data = $matches[2];
			if ($this->isUrlEncoded($data)) {
				$string = $this->convertStringEncoding(urldecode($data), $encoding, $charset);
			}
		}
		return $string;
	}

	/**
	 * Converts a string from one encoding to another.
	 * @param string $string
	 * @param string $fromEncoding
	 * @param string $toEncoding
	 * @return string Converted string if conversion was successful, or the original string if not
	 */
	protected function convertStringEncoding($string, $fromEncoding, $toEncoding) {
		$convertedString = null;
		if ($string && $fromEncoding != $toEncoding) {
			$convertedString = @iconv($fromEncoding, $toEncoding . '//IGNORE', $string);
			if (!$convertedString && extension_loaded('mbstring')) {
				$convertedString = @mb_convert_encoding($string, $toEncoding, $fromEncoding);
			}
		}
		return $convertedString ? : $string;
	}

	public function __destruct() {
		$this->disconnect();
	}

}
