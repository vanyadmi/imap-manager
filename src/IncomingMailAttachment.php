<?php

namespace Usertech\Imap;

class IncomingMailAttachment {

    public $id;
    public $name;
    public $filePath;

}
