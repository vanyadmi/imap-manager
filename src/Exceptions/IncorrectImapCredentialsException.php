<?php

namespace Usertech\Imap\Exceptions;

use Exception;

/**
 * Description of IncorrectImapCredentialsException
 *
 * @author tomas
 */
class IncorrectImapCredentialsException extends Exception {
    
}
