<?php

namespace Usertech\Imap\Exceptions;

use Exception;

/**
 * Description of IncorrectMailboxException
 *
 * @author tomas
 */
class IncorrectMailboxException extends Exception {
    
}
